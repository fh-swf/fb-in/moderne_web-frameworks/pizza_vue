const empty = {
    customer: {
        name: "",
        phone: "",
        email: ""
    },
    choice: {
        choice: "",
        size: "",
        extras: []
    },
    lieferzeit: new Date(),
}

const api = "https://jupiter.fh-swf.de/pizza/api"

const Pizza = {
    data() {
        return {
            orders: [],
            order: empty
        }
    },
    methods: {
        getOrders: function () {
            return fetch(api + "/order")
                .then(response => response.json())
                .then(orders => {
                    console.log('server response: %o', orders);
                    return orders;
                })
                .catch(error => console.log("error: %o", error));
        },
        getOrder: function (id) {
            console.log("getOrder(%s)", id);
            return fetch(`${api}/order/${id}`)
                .then(response => response.json())
                .then(order => {
                    console.log('server response: %o', order);
                    return order;
                })
                .catch(error => console.log("error: %o", error));
        },
        cancelOrder: function (event) {
            event.preventDefault();
            window.form.hidden = true;
        },
        saveOrder: function (event) {
            event.preventDefault();
            window.form.hidden = true;

            if (this.order._id) {
                let id = this.order._id;
                console.log("saving order %s", id);
                fetch(`${api}/order/${id}`, {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(this.order, function (key, value) {
                        if (key[0] === '_') {
                            return undefined;
                        } else {
                            return value;
                        }
                    })
                })
                    .then(response => response.json())
                    .then(response => {
                        console.log('server response: %o', response);
                        // vue.js does not support maps as values, so we need to search here ... 
                        for (let i = 0; i < this.orders.length; i++) {
                            if (this.orders[i]._id === id) {
                                console.log("replacing order %d with %o", i, this.order);
                                this.orders[i] = this.order;
                                break;
                            }
                        }
                    })
                    .catch(error => console.log("error: %o", error));
            } else {
                this.createOrder(event);
            }
        },
        createOrder: function (event) {
            console.log("createOrder: %o, order: %o", event, this.order);
            event.preventDefault();
            fetch(`${api}/order`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(this.order)
            })
                .then(response => response.json())
                .then(response => {
                    console.log('server response: %o', response);
                    this.orders.push(this.order);
                })
                .catch(error => console.log("error: %o", error));
        }
    },
    created: function () {
        this.getOrders().then(orders => this.orders = orders);
    }
}

const app = Vue.createApp(Pizza)


app.component("customer", {
    props: {
        modelValue: {
            name: "",
            phone: "",
            email: "",
            location: ""
        }
    },
    template: `
    <fieldset>
      <legend> Kundendaten </legend>
      <p>
        <label for="customer">Name:</label>
        <input id="customer" v-model="modelValue.name" v-on:input="handleInput" required="true" />
      </p>
      <p>
        <label for="phone">Telefon:</label>
        <input id="phone" type="tel" v-model="modelValue.phone" v-on:input="handleInput">
      </p>
      <p>
        <label for="email">Email:</label>
        <input id="email" type="email" v-model="modelValue.email" v-on:input="handleInput">
      </p>
      <p>
        <label for="location">Standort:</label>
        <select id="location" v-model="modelValue.location" v-on:input="handleInput">
          <option modelValue="hagen" accesskey="h">Hagen</option>
          <option modelValue="iserlohn" selected="true">Iserlohn</option>
          <option modelValue="meschede">Meschede</option>
          <option modelValue="soest">Soest</option>
        </select>
      </p>
    </fieldset>`,
    methods: {
        handleInput: function (event) {
            // console.log("input: %o", event);
            // console.log("modelValue: %o", this.modelValue);
            this.$emit('update:modelValue', this.modelValue);
        }
    }
});


app.component("choice", {
    props: {
        modelValue: {
            choice: "",
            size: "medium",
            extras: []
        }
    },
    template: `
  <fieldset>
    <legend>Auswahl</legend>
    <label for="choice">Gewünschte Pizza:</label>
    <input list="pizza-flavors" id="choice" v-model="modelValue.choice" v-on:input="handleInput" required="true" />
  
    <fieldset>
      <legend> Größe </legend>
      <div><label> <input type="radio" value="small" v-model="modelValue.size" v-on:input="handleInput"> Klein </label></div>
      <div><label> <input type="radio" value="medium" v-model="modelValue.size" v-on:input="handleInput"> Mittel </label></div>
      <div><label> <input type="radio" value="large" v-model="modelValue.size" v-on:input="handleInput"> Groß </label></div>
    </fieldset>
  
    <fieldset class="block">
      <legend> Extras </legend>
      <label> <input type="checkbox" value="mozzarella" v-model="modelValue.extras" v-on:input="handleInput"> Mozzarella</label>
      <label> <input type="checkbox" value="zwiebeln" v-model="modelValue.extras" v-on:input="handleInput"> Zwiebeln</label>
      <label> <input type="checkbox" value="champignons" v-model="modelValue.extras" v-on:input="handleInput"> Champignons</label>
      <label> <input type="checkbox" value="salami" v-model="modelValue.extras" v-on:input="handleInput"> Salami</label>
    </fieldset>
    <datalist id="pizza-flavors">
      <option modelValue="Margherita"/>
      <option modelValue="Funghi"/>
      <option modelValue="Salami"/>
      <option modelValue="Prosciutto"/>
      <option modelValue="Tonno"/>
    </datalist>
  </fieldset>`,
    methods: {
        handleInput: function (event) {
            console.log("input: %o", event);
            console.log("extras: %o", this.modelValue.extras);
            this.$emit('update:modelValue', this.modelValue);
        }
    }
});

app.component("lieferzeit", {
    props: {
        modelValue: {
            type: Date,
            default: new Date()
        }
    },
    data: function () {
        console.log("lieferzeit data: %o", this.modelValue);
        let iso = this.modelValue.toISOString();
        let time = this.modelValue.toLocaleTimeString().substring(0, 4);
        let date = iso.substring(0, 10);
        console.log("date: %s, time: %s", date, time);
        return {
            'time': time,
            'date': date,
            'error': null
        }
    },
    template: `
  <fieldset>
    <legend>Gewünschter Lieferzeitpunkt</legend>
    <input type="time" name="delivery_time" v-model="time" v-on:input="handleInput" />
    <input type="date" name="delivery_date" v-model="date" v-on:input="handleInput" />
    <p v-if="error" class="error">
      {{ error }}
    </p>
  </fieldset>`,
    methods: {
        handleInput: function (event) {
            console.log("date: %s, time: %s", this.date, this.time);
            let value = Date.parse(this.date + " " + this.time);
            console.log("date_time: " + value);
            this.error = null;
            if (value < Date.now()) {
                this.error = "Entschuldigen Sie bitte, aufgrund eines Problems mit unserer Zeitmachine liefern wir aktuell nicht in die Vergangenheit!";
            }
            //this.modelValue = new Date(modelValue);
            this.$emit('update:modelValue', new Date(value));
        }
    },
    watch: {
        modelValue: {
            handler: function (val, oldVal) {
                console.log("lieferzeit set: %o", val);
                let iso = this.modelValue.toISOString();
                this.time = this.modelValue.toLocaleTimeString().substring(0, 5);
                this.date = iso.substring(0, 10);
            },
            deep: true
        }
    }
});

app.component("orders", {
    template: `
    <div>
    <table id="orders">
       <tr><th>Kunde</th><th>Pizza</th><th>Lieferzeit</th></tr>
       <tr v-for="order in modelValue">
         <td>{{ order.customer.name }}</td>
         <td>{{ order.choice.choice }}</td>
         <td>{{ new Date(order.lieferzeit).toLocaleString() }}</td>
         <td><button v-on:click="showDetails" v-bind:data-id="order._id"><img src="edit-24px.svg" v-bind:data-id="order._id"/></button></td>
         <td><button v-on:click="deleteOrder" v-bind:data-id="order._id"><img src="delete-24px.svg" v-bind:data-id="order._id"/></button></td>
       </tr>
    </table>
    <button v-on:click="newOrder"><img src="create-24px.svg"/></button>
    </div>
    `,
    props: {
        modelValue: {
            type: Array
        }
    },
    methods: {
        showDetails: function (event) {
            this.$parent.getOrder(event.target.dataset.id)
                .then(order => {
                    console.log("order set to %o %o", order, this.$parent.order);
                    order.lieferzeit = new Date(order.lieferzeit);
                    console.log("parent: %o", this.$parent);
                    this.$parent.order = Vue.reactive(order);
                    window.form.hidden = false;
                });
        },
        newOrder: function (event) {
            this.$parent.order = Vue.reactive(empty);
            window.form.hidden = false;
        },

        deleteOrder: function (event) {
            let id = event.target.dataset.id;
            console.log('deleting order %s', id);
            return fetch(`${api}/order/${id}`, {
                method: 'DELETE'
            })
                .then(res => {
                    console.log('server response: %s', res.status);
                    for (let i = 0; i < this.modelValue.length; i++) {
                        if (this.modelValue[i]._id === id) {
                            console.log("deleting order #%d", i);
                            this.modelValue.splice(i, 1);
                        }
                    }
                })
                .catch(error => console.log("error: %o", error));
        },
    }
});

app.mount('#app')
